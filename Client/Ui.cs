﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using Infrastructure;
using Network;
using Type = Infrastructure.Type;

namespace Client
{
    public partial class Ui : Form
    {
        private static System.Threading.Timer _timer;
        private readonly ConcurrentDictionary<string, bool> _categories = new ConcurrentDictionary<string, bool>();
        private readonly ConcurrentQueue<string> _inQueue = new ConcurrentQueue<string>();
        private readonly INetwork _network;
        private readonly ConcurrentQueue<string> _outQueue = new ConcurrentQueue<string>();

        private readonly List<Tuple<string, string, string, string>> _tuples =
            new List<Tuple<string, string, string, string>>();

        private readonly ConcurrentDictionary<string, bool> _users = new ConcurrentDictionary<string, bool>();

        public Ui()
        {
            InitializeComponent();
            _network = TcpNetwork.Instance;
            _network.Listen(BrokerListener, NetworkDefaults.InPortClient, NetworkDefaults.MaxConnections);

            MessageListView.Columns.Add("Time", 70);
            MessageListView.Columns.Add("Category", 70);
            MessageListView.Columns.Add("Owner", 70);
            MessageListView.Columns.Add("Message", MessageListView.Width - 200);
            MessageListView.View = View.Details;

            BrokerIpAddress.Text = Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork)?.ToString();
            UserComboBox.DrawMode = DrawMode.OwnerDrawVariable;
            CategoryComboBox.DrawMode = DrawMode.OwnerDrawVariable;

            _timer = new System.Threading.Timer(delegate
            {
                Send();
                AnalyzeMessage();
            }, null, 0, 100);
        }

        private string UserName { get; set; }
        private Format Format { get; set; }

        private void BrokerListener(string message, IPAddress address)
        {
            if (MessageListView.InvokeRequired)
            {
                var d = new SetMessageCallback(BrokerListener);
                Invoke(d, message, address);
            }
            else
            {
                if (message != null)
                    _inQueue.Enqueue(message);
            }
        }

        private void Send()
        {
            while (!_outQueue.IsEmpty)
            {
                if (!_outQueue.TryPeek(out var item)) continue;

                var status = _network.Send(item, NetworkDefaults.OutPortClient, IPAddress.Parse(BrokerIpAddress.Text));
                if (status != NetworkStatus.Ok)
                {
                    ConnectionListener(status);
                }
                else
                {
                    _outQueue.TryDequeue(out item);
                    ConnectionListener(status);
                }
            }
        }

        private void ConnectionListener(NetworkStatus status)
        {
            if (MessageListView.InvokeRequired)
            {
                var d = new SetConnectionCallback(ConnectionListener);
                Invoke(d, status);
            }
            else
            {
                if (status != NetworkStatus.Ok)
                    ErrorLabel.Text = Enum.GetNames(typeof(NetworkStatus)).ToList()[(int)status];
            }
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            switch (FormatComboBox.SelectedIndex)
            {
                //case (int)Format.Json:
                //    {
                //        Format = Format.Json;
                //    }
                //    break;
                case (int)Format.Xml:
                    {
                        Format = Format.Xml;
                    }
                    break;
                default:
                    Format = Format.Json;
                    break;
            }

            if (string.IsNullOrEmpty(NickNameTextBox.Text) || string.IsNullOrEmpty(BrokerIpAddress.Text)) return;

            UserName = NickNameTextBox.Text;
            var text = Factory.Assemble(Format, Type.Connect, string.Empty, UserName, string.Empty);
            _outQueue.Enqueue(text);

            LoginPanel.Hide();
            MessengerPanel.Show();
        }

        private void MessageTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter || string.IsNullOrEmpty(MessageTextBox.Text)) return;

            string message = null;
            if (CategoryCheckBox.Checked)
            {
                if (!string.IsNullOrEmpty(CategoryComboBox.Text))
                    message = Factory.Assemble(Format, Type.Publish, MessageTextBox.Text, UserName,
                        CategoryComboBox.Text);
            }
            else
            {
                message = Factory.Assemble(Format, Type.Publish, MessageTextBox.Text, UserName, string.Empty);
            }

            if (message != null)
                _outQueue.Enqueue(message);
            MessageTextBox.Text = string.Empty;
        }

        private void OptionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MessageListView.InvokeRequired)
            {
                var d = new SetMessageListViewCallback(OptionComboBox_SelectedIndexChanged);
                Invoke(d, sender, e);
            }
            else
            {
                MessageListView.Items.Clear();

                // 1 time
                // 2 category
                // 3 author
                // 4 content

                switch (OptionComboBox.Text)
                {
                    case "Category":
                        {
                            _tuples.Where(x => !string.IsNullOrEmpty(x.Item2) && x.Item2 == CategoryComboBox.Text).ToList()
                                .ForEach(c =>
                                {
                                    MessageListView.Items.Add(new ListViewItem(new[]
                                        {c.Item1, c.Item2, c.Item3, c.Item4}));
                                });
                        }
                        break;
                    case "Category & Publishers":
                        {
                            _tuples.Where(x => !string.IsNullOrEmpty(x.Item2) && x.Item2 == CategoryComboBox.Text
                                                                              && _users[x.Item3]).ToList().ForEach(c =>
                            {
                                MessageListView.Items.Add(new ListViewItem(new[] { c.Item1, c.Item2, c.Item3, c.Item4 }));
                            });
                        }
                        break;
                    case "Publishers":
                        {
                            _tuples.Where(x => string.IsNullOrEmpty(x.Item2)).ToList().ForEach(c =>
                                {
                                    MessageListView.Items.Add(new ListViewItem(new[] { c.Item1, c.Item2, c.Item3, c.Item4 }));
                                });
                        }
                        break;
                }

                MessageListView.Refresh();
            }
        }

        private void UserComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            var text = ((ComboBox)sender).Items[e.Index].ToString();

            var brush = _users[text] ? Brushes.Green : Brushes.Red;

            e.Graphics.DrawString(text, ((Control)sender).Font, brush, e.Bounds.X, e.Bounds.Y);
        }

        private void CategoryComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            var text = ((ComboBox)sender).Items[e.Index].ToString();

            var brush = _categories[text] ? Brushes.Green : Brushes.Red;

            e.Graphics.DrawString(text, ((Control)sender).Font, brush, e.Bounds.X, e.Bounds.Y);
        }

        private void UserUpdateButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(UserComboBox.Text) || !_users.ContainsKey(UserComboBox.Text)) return;

            var message = Factory.Assemble(Format, _users[UserComboBox.Text]
                ? Type.UserUnsubscribe
                : Type.UserSubscribe, UserComboBox.Text, UserName, string.Empty);
            _outQueue.Enqueue(message);

            _users.TryGetValue(UserComboBox.Text, out var result);
            _users[UserComboBox.Text] = !result;

            UserComboBox.Refresh();
        }

        private void CategoryUpdateButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CategoryComboBox.Text)) return;

            string message;
            if (_categories.ContainsKey(CategoryComboBox.Text))
            {
                message = Factory.Assemble(Format, _categories[CategoryComboBox.Text]
                    ? Type.CategoryUnsubscribe
                    : Type.CategorySubscribe, CategoryComboBox.Text, UserName, string.Empty);

                _categories.TryGetValue(CategoryComboBox.Text, out var result);
                _categories[CategoryComboBox.Text] = !result;

                CategoryComboBox.Refresh();
            }
            else
                message = Factory.Assemble(Format, Type.CategorySubscribe, CategoryComboBox.Text, UserName,
                    string.Empty);

            _outQueue.Enqueue(message);
        }

        public void AnalyzeMessage()
        {
            if (MessageListView.InvokeRequired)
            {
                Invoke(new SetInMessagesCallback(AnalyzeMessage));
            }
            else
            {
                if (!_inQueue.TryDequeue(out var text)) return;
                if (text == null) return;
                var message = Factory.Disassemble(text);
                if (message == null) return;

                if (message.Item1.Type == Type.Syncronize)
                {
                    var syncObj = Factory.Disassemble<SyncData>
                        (message.Item1.Format, message.Item2.MessageContent);

                    _users.Clear();
                    UserComboBox.Items.Clear();

                    foreach (var keyValuePair in syncObj.Users.Dictionary)
                    {
                        _users.TryAdd(keyValuePair.Key, keyValuePair.Value);
                        UserComboBox.Items.Add(keyValuePair.Key);
                    }

                    _categories.Clear();
                    CategoryComboBox.Items.Clear();
                    foreach (var keyValuePair in syncObj.Categories.Dictionary)
                    {
                        _categories.TryAdd(keyValuePair.Key, keyValuePair.Value);
                        CategoryComboBox.Items.Add(keyValuePair.Key);
                    }

                    syncObj.UndeliveredMessages.ForEach(x =>
                    {
                        var time = DateTime.Now.ToShortTimeString();
                        _tuples.Add(new Tuple<string, string, string, string>(time, x.Category, x.Author, x.MessageContent));
                    });
                }
                else
                {
                    var time = DateTime.Now.ToShortTimeString();
                    _tuples.Add(new Tuple<string, string, string, string>(time, message.Item2.Category,
                        message.Item2.Author, message.Item2.MessageContent));
                }

                OptionComboBox_SelectedIndexChanged(null, null);
            }
        }

        private delegate void SetMessageCallback(string message, IPAddress address);

        private delegate void SetConnectionCallback(NetworkStatus status);

        private delegate void SetMessageListViewCallback(object sender, EventArgs e);

        private delegate void SetInMessagesCallback();

        private void Ui_FormClosing(object sender, FormClosingEventArgs e)
        {
            var message = Factory.Assemble(Format, Type.Disconnect, string.Empty, UserName, string.Empty);
            _network.Send(message, NetworkDefaults.OutPortClient, IPAddress.Parse(BrokerIpAddress.Text));
        }
    }
}