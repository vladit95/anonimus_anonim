﻿namespace Client
{
    partial class Ui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoginPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.BrokerIpAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FormatComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NickNameTextBox = new System.Windows.Forms.TextBox();
            this.LoginButton = new System.Windows.Forms.Button();
            this.MessengerPanel = new System.Windows.Forms.Panel();
            this.CategoryCheckBox = new System.Windows.Forms.CheckBox();
            this.OptionComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.UserUpdateButton = new System.Windows.Forms.Button();
            this.CategoryUpdateButton = new System.Windows.Forms.Button();
            this.MessageListView = new System.Windows.Forms.ListView();
            this.UserComboBox = new System.Windows.Forms.ComboBox();
            this.CategoryComboBox = new System.Windows.Forms.ComboBox();
            this.MessageTextBox = new System.Windows.Forms.TextBox();
            this.LoginPanel.SuspendLayout();
            this.MessengerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LoginPanel
            // 
            this.LoginPanel.Controls.Add(this.label3);
            this.LoginPanel.Controls.Add(this.BrokerIpAddress);
            this.LoginPanel.Controls.Add(this.label2);
            this.LoginPanel.Controls.Add(this.FormatComboBox);
            this.LoginPanel.Controls.Add(this.label1);
            this.LoginPanel.Controls.Add(this.NickNameTextBox);
            this.LoginPanel.Controls.Add(this.LoginButton);
            this.LoginPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoginPanel.Location = new System.Drawing.Point(0, 0);
            this.LoginPanel.Name = "LoginPanel";
            this.LoginPanel.Size = new System.Drawing.Size(800, 450);
            this.LoginPanel.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(374, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Broker\'s address";
            // 
            // BrokerIpAddress
            // 
            this.BrokerIpAddress.Location = new System.Drawing.Point(356, 229);
            this.BrokerIpAddress.Name = "BrokerIpAddress";
            this.BrokerIpAddress.Size = new System.Drawing.Size(121, 20);
            this.BrokerIpAddress.TabIndex = 5;
            this.BrokerIpAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(392, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Format";
            // 
            // FormatComboBox
            // 
            this.FormatComboBox.FormattingEnabled = true;
            this.FormatComboBox.Items.AddRange(new object[] {
            "Json",
            "Xml"});
            this.FormatComboBox.Location = new System.Drawing.Point(356, 177);
            this.FormatComboBox.Name = "FormatComboBox";
            this.FormatComboBox.Size = new System.Drawing.Size(121, 21);
            this.FormatComboBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(392, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "NickName";
            // 
            // NickNameTextBox
            // 
            this.NickNameTextBox.Location = new System.Drawing.Point(356, 133);
            this.NickNameTextBox.Name = "NickNameTextBox";
            this.NickNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.NickNameTextBox.TabIndex = 1;
            this.NickNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LoginButton
            // 
            this.LoginButton.Location = new System.Drawing.Point(377, 265);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(75, 23);
            this.LoginButton.TabIndex = 0;
            this.LoginButton.Text = "Log in";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // MessengerPanel
            // 
            this.MessengerPanel.Controls.Add(this.CategoryCheckBox);
            this.MessengerPanel.Controls.Add(this.OptionComboBox);
            this.MessengerPanel.Controls.Add(this.label6);
            this.MessengerPanel.Controls.Add(this.label5);
            this.MessengerPanel.Controls.Add(this.ErrorLabel);
            this.MessengerPanel.Controls.Add(this.label4);
            this.MessengerPanel.Controls.Add(this.UserUpdateButton);
            this.MessengerPanel.Controls.Add(this.CategoryUpdateButton);
            this.MessengerPanel.Controls.Add(this.MessageListView);
            this.MessengerPanel.Controls.Add(this.UserComboBox);
            this.MessengerPanel.Controls.Add(this.CategoryComboBox);
            this.MessengerPanel.Controls.Add(this.MessageTextBox);
            this.MessengerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessengerPanel.Location = new System.Drawing.Point(0, 0);
            this.MessengerPanel.Name = "MessengerPanel";
            this.MessengerPanel.Size = new System.Drawing.Size(800, 450);
            this.MessengerPanel.TabIndex = 0;
            // 
            // CategoryCheckBox
            // 
            this.CategoryCheckBox.AutoSize = true;
            this.CategoryCheckBox.Location = new System.Drawing.Point(26, 420);
            this.CategoryCheckBox.Name = "CategoryCheckBox";
            this.CategoryCheckBox.Size = new System.Drawing.Size(15, 14);
            this.CategoryCheckBox.TabIndex = 12;
            this.CategoryCheckBox.UseVisualStyleBackColor = true;
            // 
            // OptionComboBox
            // 
            this.OptionComboBox.FormattingEnabled = true;
            this.OptionComboBox.Items.AddRange(new object[] {
            "Category",
            "Publishers",
            "Category & Publishers"});
            this.OptionComboBox.Location = new System.Drawing.Point(338, 28);
            this.OptionComboBox.Name = "OptionComboBox";
            this.OptionComboBox.Size = new System.Drawing.Size(121, 21);
            this.OptionComboBox.TabIndex = 11;
            this.OptionComboBox.Text = "Publishers";
            this.OptionComboBox.SelectedIndexChanged += new System.EventHandler(this.OptionComboBox_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(686, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Users";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(58, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Categories";
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.Location = new System.Drawing.Point(34, 402);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.ErrorLabel.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(364, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "View options";
            // 
            // UserUpdateButton
            // 
            this.UserUpdateButton.Location = new System.Drawing.Point(570, 26);
            this.UserUpdateButton.Name = "UserUpdateButton";
            this.UserUpdateButton.Size = new System.Drawing.Size(75, 23);
            this.UserUpdateButton.TabIndex = 5;
            this.UserUpdateButton.Text = "Update";
            this.UserUpdateButton.UseVisualStyleBackColor = true;
            this.UserUpdateButton.Click += new System.EventHandler(this.UserUpdateButton_Click);
            // 
            // CategoryUpdateButton
            // 
            this.CategoryUpdateButton.Location = new System.Drawing.Point(153, 26);
            this.CategoryUpdateButton.Name = "CategoryUpdateButton";
            this.CategoryUpdateButton.Size = new System.Drawing.Size(75, 23);
            this.CategoryUpdateButton.TabIndex = 4;
            this.CategoryUpdateButton.Text = "Update";
            this.CategoryUpdateButton.UseVisualStyleBackColor = true;
            this.CategoryUpdateButton.Click += new System.EventHandler(this.CategoryUpdateButton_Click);
            // 
            // MessageListView
            // 
            this.MessageListView.Location = new System.Drawing.Point(26, 53);
            this.MessageListView.Name = "MessageListView";
            this.MessageListView.Size = new System.Drawing.Size(746, 346);
            this.MessageListView.TabIndex = 3;
            this.MessageListView.UseCompatibleStateImageBehavior = false;
            // 
            // UserComboBox
            // 
            this.UserComboBox.FormattingEnabled = true;
            this.UserComboBox.Location = new System.Drawing.Point(651, 26);
            this.UserComboBox.Name = "UserComboBox";
            this.UserComboBox.Size = new System.Drawing.Size(121, 21);
            this.UserComboBox.TabIndex = 2;
            this.UserComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.UserComboBox_DrawItem);
            // 
            // CategoryComboBox
            // 
            this.CategoryComboBox.FormattingEnabled = true;
            this.CategoryComboBox.Location = new System.Drawing.Point(26, 26);
            this.CategoryComboBox.Name = "CategoryComboBox";
            this.CategoryComboBox.Size = new System.Drawing.Size(121, 21);
            this.CategoryComboBox.TabIndex = 1;
            this.CategoryComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.CategoryComboBox_DrawItem);
            // 
            // MessageTextBox
            // 
            this.MessageTextBox.Location = new System.Drawing.Point(47, 418);
            this.MessageTextBox.Name = "MessageTextBox";
            this.MessageTextBox.Size = new System.Drawing.Size(725, 20);
            this.MessageTextBox.TabIndex = 0;
            this.MessageTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MessageTextBox_KeyDown);
            // 
            // Ui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LoginPanel);
            this.Controls.Add(this.MessengerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Ui";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Ui_FormClosing);
            this.LoginPanel.ResumeLayout(false);
            this.LoginPanel.PerformLayout();
            this.MessengerPanel.ResumeLayout(false);
            this.MessengerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LoginPanel;
        private System.Windows.Forms.Panel MessengerPanel;
        private System.Windows.Forms.TextBox MessageTextBox;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.TextBox NickNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox FormatComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox BrokerIpAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CategoryComboBox;
        private System.Windows.Forms.ComboBox UserComboBox;
        private System.Windows.Forms.ListView MessageListView;
        private System.Windows.Forms.Button CategoryUpdateButton;
        private System.Windows.Forms.Button UserUpdateButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox OptionComboBox;
        private System.Windows.Forms.CheckBox CategoryCheckBox;
    }
}

