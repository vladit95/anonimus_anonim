﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class RandomTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var list1 = new List<string>{"A", "B", "C"};
            var list2 = new List<string>();

            list2.AddRange(list1);
            list1.Clear();
            Console.WriteLine(list1.Count);
            list2.ForEach(Console.WriteLine);
        }
    }
}
