﻿using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Infrastructure;
using LiteDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Storage;

namespace Tests
{
    [TestClass]
    public class StorageTests
    {
        [TestMethod]
        public void InsertingTest()
        {
            IStorage storage = new LiteDb();

            var user = new User
            {
                Nickname = "NicknameTest",
                Address = IPAddress.Any,
                Format = Format.Binary,
            };

            var usersNr = storage.GetAll<User>().Count;
            storage.Insert(user);

            var afterUsersNr = storage.GetAll<User>().Count;

            Assert.AreNotEqual(usersNr, afterUsersNr);
            storage.Remove<User>(Query.EQ("Nickname", user.Nickname));

            var afterUsersNr2 = storage.GetAll<User>().Count;

            Assert.AreEqual(usersNr, afterUsersNr2);
        }

        [TestMethod]
        public void UpdatingTest()
        {
            IStorage storage = new LiteDb();

            var user = new User
            {
                Nickname = "NicknameTest",
                Address = IPAddress.Any,
                Format = Format.Binary,
            };

            storage.Insert(user);
            var me = storage.FindBy<User>(x => x.Nickname == user.Nickname).SingleOrDefault();

            if (me != null)
            {
                me.Address = IPAddress.Broadcast;
                storage.Update(me);
                Assert.AreNotEqual(user.Address, me.Address);
            }

            storage.Remove<User>(Query.EQ("Nickname", user.Nickname));
        }
    }
}
