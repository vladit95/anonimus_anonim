﻿using System;
using Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Type = Infrastructure.Type;

namespace Tests
{
    [TestClass]
    public class FormatersTests
    {
        [TestMethod]
        public void BinaryFormaterTest()
        {
            var content = new Content
            {
                Author = "TestUser",
                Category = "TestCategory",
                MessageContent = "TestMessageContent",
            };

            Assert.IsFalse(string.IsNullOrEmpty(Factory.Assemble(Format.Binary, content)));
            Assert.AreNotEqual(Factory.Assemble(Format.Binary, content), null);
            var initialLength = Factory.Assemble(Format.Binary, content).Length;

            var message = new Message
            {
                Header = new Header
                {
                    Format = Format.Binary,
                    Type = Type.Connect
                },
                Content = Factory.Assemble(Format.Binary,content)
            };
            var text = Factory.Assemble(Format.Binary, message);
            Assert.AreNotEqual(text, null);

            var afterMessage = Factory.Disassemble<Message>(Format.Binary, text);
            Assert.AreNotEqual(afterMessage, null);

            Assert.AreEqual(initialLength, afterMessage.Content.Length);
            var afterContent = Factory.Disassemble<Content>(Format.Binary, afterMessage.Content);
            Assert.AreNotEqual(afterContent, null);
            Assert.AreEqual(content.Author, afterContent.Author);
        }


        [TestMethod]
        public void JsonFormaterTest()
        {
            var content = new Content
            {
                Author = "TestUser",
                Category = "TestCategory",
                MessageContent = "TestMessageContent",
            };

            Assert.IsFalse(string.IsNullOrEmpty(Factory.Assemble(Format.Json, content)));
            Assert.AreNotEqual(Factory.Assemble(Format.Json, content), null);

            var message = new Message
            {
                Header = new Header
                {
                    Format = Format.Json,
                    Type = Type.Connect
                },
                Content = Factory.Assemble(Format.Json, content)
            };
            var text = Factory.Assemble(Format.Json, message);
            Assert.AreNotEqual(text, null);

            var afterMessage = Factory.Disassemble<Message>(Format.Json, text);
            Assert.AreNotEqual(afterMessage, null);

            var afterContent = Factory.Disassemble<Content>(Format.Json, afterMessage.Content);
            Assert.AreNotEqual(afterContent, null);

            Assert.AreEqual(content.Author, afterContent.Author);
        }

        [TestMethod]
        public void XmlFormaterTest()
        {
            var content = new Content
            {
                Author = "TestUser",
                Category = "TestCategory",
                MessageContent = "TestMessageContent",
            };

             Assert.IsFalse(string.IsNullOrEmpty(Factory.Assemble(Format.Xml, content)));
            Assert.AreNotEqual(Factory.Assemble(Format.Xml, content), null);

            var message = new Message
            {
                Header = new Header
                {
                    Format = Format.Xml,
                    Type = Type.Connect
                },
                Content = Factory.Assemble(Format.Xml, content)
            };
            var text = Factory.Assemble(Format.Xml, message);
             Assert.AreNotEqual(text, null);

            var afterMessage = Factory.Disassemble<Message>(Format.Xml, text);
             Assert.AreNotEqual(afterMessage, null);

            var afterContent = Factory.Disassemble<Content>(Format.Xml, afterMessage.Content);
             Assert.AreNotEqual(afterContent, null);

             Assert.AreEqual(content.Author, afterContent.Author);
        }

        [TestMethod]
        public void MixedFormaterTest()
        {
            var content = new Content
            {
                Author = "TestUser",
                Category = "TestCategory",
                MessageContent = "TestMessageContent",
            };

            var message = new Message
            {
                Header = new Header
                {
                    Format = Format.Json,
                    Type = Type.Connect
                },
                Content = Factory.Assemble(Format.Json, content)
            };
            var text = Factory.Assemble(Format.Binary, message);
            Assert.AreNotEqual(text, null);
            var afterMessage = Factory.Disassemble<Message>(Format.Binary, text);
            Assert.AreNotEqual(afterMessage, null);
            var afterContent = Factory.Disassemble<Content>(Format.Json, afterMessage.Content);
            Assert.AreNotEqual(afterContent, null);
            Assert.AreEqual(content.Author, afterContent.Author);
        }
    }
}
