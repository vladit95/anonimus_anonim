﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Network;

namespace Example
{
    class Program
    {
        static void Main(string[] args)
        {
            var network = TcpNetwork.Instance;
            var ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork);
            network.Listen(Write,11001,100);
            var ip2 = IPAddress.Parse("172.31.214.196");
            //IPAddress.Parse();
            Console.WriteLine(ip);

            while (true)
            {
                var message = Console.ReadLine();
                network.Send(message, 11000, ip2);
            }
        }

        static void Write(string text, IPAddress address)
        {
            Console.WriteLine(text);
        }
    }
}
