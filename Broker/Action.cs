﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Infrastructure;
using Storage;
using Header = Infrastructure.Header;
using Type = Infrastructure.Type;

namespace Broker
{
    internal class Action
    {
        private readonly IStorage _db = new LiteDb();

        internal void AddErrorMessage(IPAddress address, Content content)
        {
            var user = _db.FindBy<User>(x => Equals(x.Address, address)).SingleOrDefault();
            if (user == null) return;
            user.UndeliveredMessages.Add(content);
            Disconnect(address);
            _db.Update(user);
        }

        internal void UserSubscribe(string publisherName, string subscriber)
        {
            var user = _db.FindBy<User>(x => x.Nickname == subscriber).SingleOrDefault();
            var publisher = _db.FindBy<User>(x => x.Nickname == publisherName).SingleOrDefault();

            if (publisher == null || user == null || user.PublishersList.Contains(publisherName)) return;
            user.PublishersList.Add(publisherName);
            _db.Update(user);
        }

        internal void UserUnsubscribe(string publisherName, string subscriber)
        {
            var user = _db.FindBy<User>(x => x.Nickname == subscriber).SingleOrDefault();
            var publisher = _db.FindBy<User>(x => x.Nickname == publisherName).SingleOrDefault();

            if (publisher == null || user == null || !user.PublishersList.Contains(publisherName)) return;
            user.PublishersList.Remove(publisherName);
            _db.Update(user);

        }

        internal List<Tuple<IPAddress, Content, Header>> CategorySubscribe(string categoryName, string subscriber)
        {
            var user = _db.FindBy<User>(x => x.Nickname == subscriber).SingleOrDefault();
            var category = _db.FindBy<Category>(x => x.Name == categoryName).SingleOrDefault();

            if (user == null) return new List<Tuple<IPAddress, Content, Header>>();
            if (!user.CategoryList.Contains(categoryName))
            {
                user.CategoryList.Add(categoryName);
                _db.Update(user);
            }

            if (category != null) return new List<Tuple<IPAddress, Content, Header>>();

            var catObj = new Category
            {
                Name = categoryName
            };
            _db.Insert(catObj);
            return Syncronize();
        }

        internal void CategoryUnsubscribe(string categoryName, string subscriber)
        {
            var user = _db.FindBy<User>(x => x.Nickname == subscriber).SingleOrDefault();
            var category = _db.FindBy<Category>(x => x.Name == categoryName).SingleOrDefault();

            if (category == null || user == null || !user.CategoryList.Contains(categoryName)) return;
            user.CategoryList.Remove(categoryName);
            _db.Update(user);
        }

        internal List<Tuple<IPAddress, Content, Header>> Syncronize()
        {
            var allUsers = _db.GetAll<User>().Select(c => c.Nickname).ToList();
            var allCategories = _db.GetAll<Category>().Select(c => c.Name).ToList();

            var list = new List<Tuple<IPAddress, Content, Header>>();

            _db.FindBy<User>(d => d.IsActive).ForEach(activeUser =>
            {
                Console.WriteLine(activeUser.Nickname);
                var tuple = StateChanged(activeUser, allUsers, allCategories);

                if(tuple != null)
                    list.Add(tuple);
            });

            return list;
        }

        internal Tuple<IPAddress, Content, Header> StateChanged(User me, List<string> allUsers, List<string> allCategories)
        {
            if (me == null) return null;
            var syncData = new SyncData
            {
                Users = new CustomDictionary<string, bool>(),
                Categories = new CustomDictionary<string, bool>(),
                UndeliveredMessages = me.UndeliveredMessages
            };

            me.UndeliveredMessages.Clear();
            _db.Update(me);

            allUsers.ForEach(item =>
            {
                syncData.Users.Add(item, me.PublishersList.Contains(item));
            });

            allCategories.ForEach(item =>
            {
                syncData.Categories.Add(item, me.CategoryList.Contains(item));
            });

            var header = new Header
            {
                Format = me.Format,
                Type = Type.Syncronize
            };

            var content = new Content
            {
                Author = string.Empty,
                Category = string.Empty,
                MessageContent = Factory.Assemble(me.Format, syncData)
            };

            return new Tuple<IPAddress, Content, Header>(me.Address, content, header);
        }

        internal List<Tuple<IPAddress, Content, Header>> Connect(IPAddress address, string nickname, Format format)
        {
            var list = new List<Tuple<IPAddress, Content, Header>>();
           
            var user = _db.FindBy<User>(x => x.Nickname == nickname).SingleOrDefault();
            if (user == null)
            {
                var obj = new User
                {
                    Address = address,
                    Nickname = nickname,
                    PublishersList = new List<string>(),
                    CategoryList = new List<string>(),
                    UndeliveredMessages = new List<Content>(),
                    Format = format,
                    IsActive = true
                };
                _db.Insert(obj);
            }
            else
            {
                user.IsActive = true;
                user.Address = address;
                user.Format = format;
                _db.Update(user);
            }
            var allUsers = _db.GetAll<User>().Select(c => c.Nickname).ToList();
            var allCategories = _db.GetAll<Category>().Select(c => c.Name).ToList();
            var tuple = StateChanged(user, allUsers, allCategories);
            return tuple != null ? new List<Tuple<IPAddress, Content, Header>> { tuple } : list;
        }

        internal void Disconnect(IPAddress address)
        {
            var user = _db.FindBy<User>(x => Equals(x.Address, address)).SingleOrDefault();
            if (user == null) return;
            user.IsActive = false;
            _db.Update(user);
        }

        internal List<Tuple<IPAddress, Content, Header>> Publish(Content content)
        {
            var list = new List<Tuple<IPAddress, Content, Header>>();
            var user = _db.FindBy<User>(x => x.Nickname == content.Author).SingleOrDefault();
            if (user == null) return list;


            var activeUsers = content.Category != string.Empty ? _db.FindBy<User>(x => x.CategoryList.Contains(content.Category) && x.IsActive) :
                _db.FindBy<User>(x => x.PublishersList.Contains(user.Nickname) && x.IsActive);

            var idleUsers = content.Category != string.Empty ? _db.FindBy<User>(x => x.CategoryList.Contains(content.Category) && !x.IsActive) :
                _db.FindBy<User>(x => x.PublishersList.Contains(user.Nickname) && !x.IsActive);

            idleUsers.ForEach(x =>
            {
                x.UndeliveredMessages.Add(content);
            });

            list.AddRange(from item in activeUsers
                          let header = new Header
                          {
                              Format = item.Format,
                              Type = Type.Publish
                          }
                          select new Tuple<IPAddress, Content, Header>(item.Address, content, header));

            return list;
        }
    }
}
