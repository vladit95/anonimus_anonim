﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Infrastructure;
using Network;
using Type = Infrastructure.Type;

namespace Broker
{
    internal class Analyzer
    {
        private readonly INetwork _network;
        private readonly Action _action = new Action();

        private readonly ConcurrentQueue<Tuple<string, IPAddress>> _inQueue;

        public Analyzer(INetwork network)
        {
            _inQueue = new ConcurrentQueue<Tuple<string, IPAddress>>();
            _network = network;
            _network.Listen(AnalyzeNewMessages, NetworkDefaults.InPortBroker, NetworkDefaults.MaxConnections);
            Work();
        }


        private void AnalyzeNewMessages(string text, IPAddress address)
        {
            _inQueue.Enqueue(new Tuple<string, IPAddress>(text, address));
        }

        private void Work()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    if (!_inQueue.TryDequeue(out var rawData) || rawData == null) continue;
                    var tuple = Factory.Disassemble(rawData.Item1);
                    SolveMessage(tuple.Item2, tuple.Item1, rawData.Item2);
                }
            });
        }

        private void SolveMessage(Content content, Header header, IPAddress address)
        {
            List<Tuple<IPAddress, Content, Header>> list = null;
            switch (header.Type)
            {
                case Type.Connect:
                    {
                        Console.WriteLine(content.Author + " has connected!");
                        list = _action.Connect(address, content.Author, header.Format);
                    }
                    break;
                case Type.Disconnect:
                    {
                        Console.WriteLine(content.Author + " has disconnected!");
                        _action.Disconnect(address);

                    }
                    break;
                case Type.Publish:
                    {
                        Console.WriteLine(content.Author + " has published!");
                        list = _action.Publish(content);
                    }
                    break;
                case Type.UserSubscribe:
                    {
                        Console.WriteLine(content.Author + " has subscribed to " + content.MessageContent);
                        _action.UserSubscribe(content.MessageContent, content.Author);
                    }
                    break;
                case Type.UserUnsubscribe:
                    {
                        Console.WriteLine(content.Author + " has unsubcribed from " + content.MessageContent);

                        _action.UserUnsubscribe(content.MessageContent, content.Author);
                    }
                    break;
                case Type.CategorySubscribe:
                    {
                        Console.WriteLine(content.Author + " has subscribed to " + content.MessageContent + " category.");
                        _action.CategorySubscribe(content.MessageContent, content.Author);
                    }
                    break;
                case Type.CategoryUnsubscribe:
                    {
                        Console.WriteLine(content.Author + " has unsubscribed from " + content.MessageContent + " category.");
                        _action.CategoryUnsubscribe(content.MessageContent, content.Author);
                    }
                    break;
            }
            CustomFormat(list);
        }

        private void CustomFormat(List<Tuple<IPAddress, Content, Header>> list) =>
            list?.ForEach(x =>
            {
                var message = Factory.Assemble(x.Item3, x.Item2);
                SendResponses(message, x.Item2, x.Item1);
            });

        private void SendResponses(string message, Content content, IPAddress address)
        {
            var status = _network.Send(message, NetworkDefaults.OutPortBroker, address);
            if (status != NetworkStatus.Ok)
            {
                _action.AddErrorMessage(address, content);
                Console.WriteLine("Saved as undelivered message -> " + status);
            }
            else
            {
                Console.WriteLine("Sent");
            }
        }
    }
}
