﻿using System;

namespace Infrastructure
{
    public enum Format { Binary, Json, Xml}
    public enum Type { Connect, Disconnect, Publish, Syncronize, CategorySubscribe,
                    CategoryUnsubscribe, UserSubscribe, UserUnsubscribe}

    [Serializable]
    public class Message
    {
        public Header Header { get; set; }
        public string Content { get; set; }
    }

    [Serializable]
    public class Header
    {
        public Format Format { get; set; }
        public Type Type { get; set; }
    }

    [Serializable]
    public class Content
    {
        public string MessageContent { get; set; }
        public string Category { get; set; }
        public string Author { get; set; }
    }
}
