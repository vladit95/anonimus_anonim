﻿using System.Collections.Generic;
using System.Net;
using System.Xml.Serialization;
using LiteDB;

namespace Infrastructure
{
    public class User
    {
        [BsonId]
        public int Id { get; set; }
        public string Nickname { get; set; }

        [XmlIgnore]
        [BsonIgnore]
        public IPAddress Address { get; set; }
        public List<string> PublishersList { get; set; }
        public List<Content> UndeliveredMessages { get; set; }
        public List<string> CategoryList { get; set; }
        public Format Format { get; set; }
        public bool IsActive { get; set; }

        [XmlElement("Address")]
        public string AddressForXml
        {
            get => Address.ToString();
            set => Address = string.IsNullOrEmpty(value) ? null : IPAddress.Parse(value);
        }
    }
}
