﻿using System;
using System.Collections.Generic;

namespace Infrastructure
{
    [Serializable]
    public class SyncData
    {
        public CustomDictionary<string,bool> Users { get; set; }
        public CustomDictionary<string,bool> Categories { get; set; }
        public List<Content> UndeliveredMessages { get; set; }
    }
}
