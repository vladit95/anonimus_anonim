﻿using System;
using System.Collections.Generic;

namespace Infrastructure
{
    [Serializable]
    public class CustomDictionary<T,TU>
    {
        public List<KeyPair<T,TU>> Dictionary = new List<KeyPair<T, TU>>();

        public void Add(T key, TU value)
        {
            Dictionary.Add(new KeyPair<T, TU>(key,value));
        }
    }
}
