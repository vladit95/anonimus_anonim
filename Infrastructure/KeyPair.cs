﻿using System;

namespace Infrastructure
{
    [Serializable]
    public class KeyPair<T,TU>
    {
        public T Key { get; set; }
        public TU Value { get; set; }

        public KeyPair(T key, TU value)
        {
            Key = key;
            Value = value;
        }

        public KeyPair()
        {

        }
    }
}
