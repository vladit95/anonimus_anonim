﻿using System;
using Infrastructure.Formaters;

namespace Infrastructure
{
    public class Factory
    {
        public static string Assemble<T>(Format format, T t)
        {
            return GetFormat(format).Serialize(t);
        }

        public static T Disassemble<T>(Format format, string text)
        {
            return GetFormat(format).Deserialize<T>(text);
        }

        public static string Assemble(Header header, Content content)
        {
            var message = new Message
            {
                Header = header,
                Content = GetFormat(header.Format).Serialize(content)
            };
            //return Assemble(Format.Binary,message);
            return Assemble(Format.Json, message);
        }

        public static string Assemble(Format format, Type type, string contentt, string author, string category)
        {
            var content = new Content
            {
                MessageContent = contentt,
                Category = category,
                Author = author
            };

            var header = new Header
            {
                Format = format,
                Type = type
            };

            var message = new Message
            {
                Header = header,
                Content = GetFormat(format).Serialize(content)
            };

            //return Assemble(Format.Binary, message);
            return Assemble(Format.Json, message);
        }


        public static Tuple<Header, Content> Disassemble(string messageString)
        {
            //var message = Disassemble<Message>(Format.Binary, messageString);
            var message = Disassemble<Message>(Format.Json, messageString);

            if (message == null) return null;
            var content = Disassemble<Content>(message.Header.Format, message.Content);
            return content == null ? null : new Tuple<Header, Content>(message.Header, content);
        }

        public static IFormater GetFormat(Format format)
        {
            switch (format)
            {
                case Format.Xml:
                {
                    return new Xml();
                }
                case Format.Json:
                {
                    return new Json();
                }
                default:
                {
                    return new Binary();
                }
            }
        }
    }
}
