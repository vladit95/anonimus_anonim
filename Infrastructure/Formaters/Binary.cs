﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Infrastructure.Formaters
{
    public class Binary : IFormater
    {
        public string Serialize<T>( T obj)
        {
            if (obj == null) return null;
            try
            {
                byte[] array;
                
                using (var memoryStream = new MemoryStream())
                {
                    new BinaryFormatter().Serialize(memoryStream, obj);
                    array = memoryStream.ToArray();
                    memoryStream.Flush();
                }

              //  return Encoding.GetEncoding(1251).GetString(array);
                return Encoding.ASCII.GetString(array);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public T Deserialize<T>(string message)
        {
            if (string.IsNullOrEmpty(message)) return default(T);

            try
            {
                T obj;
                using (var memoryStream = new MemoryStream(/*Encoding.GetEncoding(1251).GetBytes(message)*/ Encoding.ASCII.GetBytes(message)))
                {
                    memoryStream.Position = 0;
                    obj = (T)new BinaryFormatter().Deserialize(memoryStream);
                }

                return obj;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
    }
}
