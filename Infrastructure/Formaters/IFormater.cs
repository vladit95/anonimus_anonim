﻿namespace Infrastructure.Formaters
{
    public interface IFormater
    {
        T Deserialize<T>(string message);
        string Serialize<T>(T obj);
    }
}