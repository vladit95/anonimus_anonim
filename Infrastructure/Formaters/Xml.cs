﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Infrastructure.Formaters
{
    public class Xml : IFormater
    {
        public string Serialize<T>(T obj)
        {
            if (obj == null) return null;

            try
            {
                var serializer = new XmlSerializer(typeof(T));
                var sb = new StringBuilder();

                using (TextWriter writer = new StringWriter(sb))
                {
                    serializer.Serialize(writer, obj);
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public T Deserialize<T>(string message)
        {
            if (string.IsNullOrEmpty(message)) return default(T);

            var serializer = new XmlSerializer(typeof(T));
            try
            {
                T result;
                using (TextReader reader = new StringReader(message))
                {
                    result = (T)serializer.Deserialize(reader);
                }
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
    }
}
