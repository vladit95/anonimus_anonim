﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Infrastructure.Formaters
{
    public class Json : IFormater
    {
        public string Serialize<T>(T obj)
        {
            try
            {
                var msObj = new MemoryStream();
                new DataContractJsonSerializer(typeof(T)).WriteObject(msObj, obj);
                msObj.Position = 0;
                var sr = new StreamReader(msObj);
                var json = sr.ReadToEnd();
                sr.Close();
                msObj.Close();
                return json;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
            
        }

        public T Deserialize<T>(string message)
        {
            if (string.IsNullOrEmpty(message)) return default(T);

            try
            {
                T obj;
                var deserializer = new DataContractJsonSerializer(typeof(T));
                using (var ms = new MemoryStream(Encoding.ASCII.GetBytes(message)))
                {
                    obj = (T)deserializer.ReadObject(ms);
                }

                return obj;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
    }
}
