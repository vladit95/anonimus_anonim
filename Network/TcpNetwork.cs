﻿using System;
using System.Net;
using System.Threading.Tasks;
using Network.TCP;

namespace Network
{
    public class TcpNetwork : INetwork
    {
        private Server _server;
        private static readonly Lazy<TcpNetwork> Lazy = new Lazy<TcpNetwork>(() => new TcpNetwork());
        public static TcpNetwork Instance => Lazy.Value;
        private bool _serverCreated;
        private static Client _client;
        private int _port;
        private int _maxConnQueue;

        private TcpNetwork()
        {
            _client = new Client(SetClientStatus);
        }

        public NetworkStatus Send(string message, int port, IPAddress address)
        {
            //if(ClientStatus != NetworkStatus.Ok)
            //    _client = new Client(SetClientStatus);
            _client.StartClient(message, port, address);
            return ClientStatus;
        }

        public void Listen(Action<string, IPAddress> responAction, int port, int maxConnectionQueue)
        {
            if (_serverCreated) return;
            _server = new Server(responAction, port, maxConnectionQueue);
            //_port = port;
            //_maxConnQueue = maxConnectionQueue;
            _serverCreated = true;
            Task.Run(() =>
            {
                _server.StartListening(SetServerStatus);
            });
        }

        private void SetServerStatus(NetworkStatus status)
        {
            ServerStatus = status;
        }

        private void SetClientStatus(NetworkStatus status)
        {
            ClientStatus = status;
        }

        public NetworkStatus ClientStatus { get; set; }
        public NetworkStatus ServerStatus { get; set; }
    }
}