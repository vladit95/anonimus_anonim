﻿using System;
using System.Net;

namespace Network
{
    public enum NetworkStatus { Ok, SendError, ReceiveError}
    public interface INetwork
    {
        NetworkStatus Send(string message, int port, IPAddress address);
        void Listen(Action<string, IPAddress> responAction, int port, int maxConnectionQueue);

        NetworkStatus ClientStatus { get; set; }
        NetworkStatus ServerStatus { get; set; }
    }
}
