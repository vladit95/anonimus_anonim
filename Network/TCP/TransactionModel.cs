﻿using System.Net.Sockets;
using System.Text;

namespace Network.TCP
{
    internal class TransationModel
    {
        public const int BufferSize = 4096;
        public byte[] Buffer = new byte[BufferSize];
        public StringBuilder Sb = new StringBuilder();
        public Socket WorkSocket;
    }
}
