﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Network.TCP
{
    public class Server
    {
        private bool _isListening;
        public ManualResetEvent AllDone = new ManualResetEvent(false);
        private readonly int _port;
        private readonly int _maxConnectionQueue;
        private readonly Action<string, IPAddress> _responseAction;

        public Server(Action<string, IPAddress> responseAction, int port, int maxConnectionQueue)
        {
            _port = port;
            _maxConnectionQueue = maxConnectionQueue;
            _responseAction = responseAction;
            _isListening = true;
        }

        public void StartListening(Action<NetworkStatus> action)
        {
            var ipAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork);

            var localEndPoint = new IPEndPoint(IPAddress.Any, _port);

            if (ipAddress != null)
            {
                var listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(_maxConnectionQueue);

                    while (_isListening)
                    {
                        AllDone.Reset();
                        //Console.WriteLine("Waiting for a connection...");
                        listener.BeginAccept(AcceptCallback, listener);
                        AllDone.WaitOne();
                    }
                }
                catch (Exception e)
                {
                    action(NetworkStatus.ReceiveError);
                    Console.WriteLine(e.ToString());
                }
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            AllDone.Set();

            var listener = (Socket)ar.AsyncState;
            var handler = listener.EndAccept(ar);

            var state = new TransationModel { WorkSocket = handler };
            handler.BeginReceive(state.Buffer, 0, TransationModel.BufferSize, 0, ReadCallback, state);
        }

        private void ReadCallback(IAsyncResult ar)
        {
            var state = (TransationModel)ar.AsyncState;
            var handler = state.WorkSocket;

            var bytesRead = handler.EndReceive(ar);

            if (bytesRead <= 0) return;
            state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

            var content = state.Sb.ToString();
            if (content.IndexOf("<EOF>", StringComparison.Ordinal) > -1)
            {
                _responseAction(content.Replace("<EOF>", ""), (handler.RemoteEndPoint as IPEndPoint)?.Address);
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            else
            {
                handler.BeginReceive(state.Buffer, 0, TransationModel.BufferSize, 0, ReadCallback, state);
            }
        }

        public void Close()
        {
            _isListening = false;
        }
    }
}