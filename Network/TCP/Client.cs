﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Network.TCP
{
    public class Client
    {
        private readonly ManualResetEvent _connectDone = new ManualResetEvent(false);
        private readonly ManualResetEvent _sendDone = new ManualResetEvent(false);
        private readonly ManualResetEvent _receiveDone = new ManualResetEvent(false);
        private static Action<NetworkStatus> _action;

        public Client(Action<NetworkStatus> action)
        {
            _action = action;
        }

        public void StartClient(string message, int port, IPAddress address)
        {
            try
            {
                var remoteEp = new IPEndPoint(address, port);
                var client = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                var a = client.BeginConnect(remoteEp, ConnectCallback, client);
                bool success = a.AsyncWaitHandle.WaitOne(5000, true);
                _connectDone.WaitOne();
                Send(client, message + "<EOF>");
                _sendDone.WaitOne();
                _receiveDone.Set();
                _receiveDone.WaitOne();
            }
            catch (Exception e)
            {
                _action(NetworkStatus.SendError);
                Console.WriteLine(e.ToString());
            }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                var client = (Socket) ar.AsyncState;
                client.EndConnect(ar);
                _connectDone.Set();
            }
            catch (Exception e)
            {
                _action(NetworkStatus.SendError);
                Console.WriteLine(e.ToString());
            }
        }

        private void Send(Socket client, string data)
        {
            var byteData = Encoding.ASCII.GetBytes(data);

            client.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, client);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                ((Socket)ar.AsyncState).EndSend(ar);
                _sendDone.Set();


                //////////////////////////////////////////////////////////////
                //var handler = (ar.AsyncState as TransationModel)?.WorkSocket;
                //if (handler == null) return;
                //handler.Shutdown(SocketShutdown.Both);
                //handler.Close();
            }
            catch (Exception e)
            {
                _action(NetworkStatus.SendError);
                Console.WriteLine(e.ToString());
            }
        }
    }
}