﻿namespace Network
{
    public class NetworkDefaults
    {
        public static readonly int InPortClient = 11000;
        public static readonly int OutPortClient = 11001;

        public static readonly int InPortBroker = 11001;
        public static readonly int OutPortBroker = 11000;

        public static readonly int MaxConnections = 100;
    }
}
